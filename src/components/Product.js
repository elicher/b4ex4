import React from 'react'

function Product(props) {
  const { product, onAdd } = props
  return (
    <div>
      <img className="small" src={product.image} alt={product.product}/>
      <h3>{product.product}</h3>
      <div>${product.price}</div>
      <div>
        <button onClick={()=>onAdd(product)} >Add to cart</button>
      </div>
    </div>
  )
}

export default Product