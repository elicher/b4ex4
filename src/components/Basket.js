import React from 'react';

function Basket (props) {
    const { cartItems, onAdd, onRemove } = props
    const itemsPrice = cartItems.reduce((a,c) => a+c.price*c.qty,0)
    const shippingPrice = 20;
return(
    <aside className="block col-1">
        <h2>Cart Items</h2>
    <div>
        {cartItems.length==0 && <div>Cart is Empty</div>}
        {cartItems.map((item)=>(
            <div key={item.id} className="row">
                <div className="col-2">{item.product}</div>
                <div className="col-2">
                 <button onClick={()=>onAdd(item)} className="add">+</button>
                 <button onClick={()=>onRemove(item)} className="add">-</button>
                </div>
                <div className="col-2 text-right">
                    {item.qty} x ${item.price.toFixed(2)}
                    </div>
            </div>
    ))}
    </div>
    {itemsPrice > 500 ? <p style={{color:"green"}}> You get free shipping! </p> : <p style={{color:"red"}}> + shippingPrice</p>}
    <p>TOTAL: {itemsPrice}</p>
    </aside>
)
}

export default Basket